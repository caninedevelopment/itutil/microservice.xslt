﻿// <copyright file="Controller.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.XSLT
{
    using System;
    using System.Collections.Generic;
    using ITUtil.Common.DTO;
    using ITUtil.Common.Utils;
    using ITUtil.Common.Utils.Bootstrapper;
    using static ITUtil.Common.Utils.Bootstrapper.OperationDescription;

    /// <summary>
    /// Controller
    /// </summary>
    public class Controller : IMicroservice
    {
        private static Database_v1 db = new Database_v1();

        /// <inheritdoc/>
        public string ServiceName
        {
            get
            {
                return "XSLT";
            }
        }

        /// <inheritdoc/>
        public ProgramVersion ProgramVersion
        {
            get
            {
                return new ProgramVersion("1.0.0.0");
            }
        }

        private static readonly MetaDataDefinition Admin = new MetaDataDefinition("Monosoft.Service.XSLT", "admin", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("en", "administrators are allowed to define xslt logic"));
        private static MetaDataDefinition sysoperator = new MetaDataDefinition("Monosoft.Service.XSLT", "operator", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("en", "operators are allowed to run xslt logic"));

        /// <summary>
        /// Gets OperationDescription
        /// </summary>
        public List<OperationDescription> OperationDescription
        {
            get
            {
                return new List<OperationDescription>()
                {
                   new OperationDescription(
                       "create",
                       fanoutType.perMachine,
                       "Create a XSL template",
                       typeof(ITUtil.XSLT.DTO.XSLTemplate),
                       null,
                       new MetaDataDefinitions(new MetaDataDefinition[] { Admin }),
                       CreateXSLTemplate),
                   new OperationDescription(
                       "update",
                       fanoutType.perMachine,
                       "Update an existing XSL template",
                       typeof(ITUtil.XSLT.DTO.XSLTemplate),
                       null,
                       new MetaDataDefinitions(new MetaDataDefinition[] { Admin }),
                       UpdateXSLTemplate),
                   new OperationDescription(
                       "delete",
                       fanoutType.perMachine,
                       "Deletes an existing XSL template",
                       typeof(ITUtil.XSLT.DTO.XSLTemplate),
                       null,
                       new MetaDataDefinitions(new MetaDataDefinition[] { Admin }),
                       DeleteXSLTemplate),
                   new OperationDescription(
                       "read",
                       fanoutType.none,
                       "Gets an existing XSL template",
                       null,
                       typeof(ITUtil.XSLT.DTO.XSLTemplates),
                       new MetaDataDefinitions(new MetaDataDefinition[] { Admin }),
                       ReadXSLTemplate),
                   new OperationDescription(
                       "Transform",
                       fanoutType.none,
                       "Run a XSL template on a specific XML content",
                       typeof(ITUtil.XSLT.DTO.XMLInput),
                       typeof(ITUtil.XSLT.DTO.XML),
                       new MetaDataDefinitions(new MetaDataDefinition[] { sysoperator }),
                       GenerateXML),
                };
            }
        }

        /// <summary>
        /// CreateXSLTemplate
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper CreateXSLTemplate(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var input = MessageDataHelper.FromMessageData<ITUtil.XSLT.DTO.XSLTemplate>(wrapper.messageData);
            if (db.CreateXSLTemplate(input))
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Template already exists"));
            }
        }

        /// <summary>
        /// UpdateXSLTemplate
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper UpdateXSLTemplate(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var input = MessageDataHelper.FromMessageData<ITUtil.XSLT.DTO.XSLTemplate>(wrapper.messageData);
            if (db.UpdateXSLTemplate(input))
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Template does not exists"));
            }
        }

        /// <summary>
        /// DeleteXSLTemplate
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper DeleteXSLTemplate(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var input = MessageDataHelper.FromMessageData<ITUtil.XSLT.DTO.GetXSLTemplate>(wrapper.messageData);
            if (db.DeleteXSLTemplate(input))
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Template does not exists"));
            }
        }

        /// <summary>
        /// ReadXSLTemplate
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ReadXSLTemplate(MessageWrapper wrapper)
        {
            var res = db.ReadXSLTemplate();
            if (res != null)
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, res, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Template does not exists"));
            }
        }

        /// <summary>
        /// GenerateXML
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper GenerateXML(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var input = MessageDataHelper.FromMessageData<ITUtil.XSLT.DTO.XMLInput>(wrapper.messageData);
            var res = db.GenerateXML(input);

            if (res != null)
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, res, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Template does not exists"));
            }
        }
    }
}
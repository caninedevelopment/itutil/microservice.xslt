﻿// <copyright file="Database_v1.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.XSLT
{
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.IO;
    using System.Xml;

    /// <summary>
    /// Database_v1.
    /// </summary>
    public class Database_v1
    {
        private static string DBFILENAME = "xsltdb.sqlite";
        private SQLiteConnection m_dbConnection = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Database_v1"/> class.
        /// </summary>
        public Database_v1()
        {
            if (this.m_dbConnection == null)
            {
                if (System.IO.File.Exists(DBFILENAME) == false)
                {
                    SQLiteConnection.CreateFile(DBFILENAME);
                    this.m_dbConnection = new SQLiteConnection("Data Source=xsltdb.sqlite;Version=3;");
                    this.m_dbConnection.Open();
                    var cmd = new SQLiteCommand("CREATE TABLE XSLTemplate (id varchar(28) primary key, name varchar(200), template varchar)", this.m_dbConnection);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    this.m_dbConnection = new SQLiteConnection("Data Source=xsltdb.sqlite;Version=3;");
                    this.m_dbConnection.Open();
                }
            }
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="Database_v1"/> class.
        /// </summary>
        ~Database_v1()
        {
            this.m_dbConnection.Close();
        }

        /// <summary>
        /// CreateXSLTemplate
        /// </summary>
        /// <param name="input">ITUtil.XSLT.DTO.XSLTemplate input</param>
        /// <returns>bool</returns>
        public bool CreateXSLTemplate(ITUtil.XSLT.DTO.XSLTemplate input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            var cmd = new SQLiteCommand("SELECT id FROM XSLTemplate WHERE id=@id", this.m_dbConnection);
            cmd.Parameters.AddWithValue("@id", input.id.ToString("N", ITUtil.Common.Constants.Culture.DefaultCulture));
            var dbresult = cmd.ExecuteScalar();
            cmd.Dispose();
            if (dbresult == null)
            {
                cmd = new SQLiteCommand("INSERT INTO XSLTemplate (id, name, template) values (@id, @name, @template)", this.m_dbConnection);
                cmd.Parameters.AddWithValue("@id", input.id.ToString("N", ITUtil.Common.Constants.Culture.DefaultCulture));
                cmd.Parameters.AddWithValue("@name", input.name);
                cmd.Parameters.AddWithValue("@template", input.template);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// UpdateXSLTemplate
        /// </summary>
        /// <param name="input">ITUtil.XSLT.DTO.XSLTemplate input</param>
        /// <returns>bool</returns>
        public bool UpdateXSLTemplate(ITUtil.XSLT.DTO.XSLTemplate input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            var cmd = new SQLiteCommand("SELECT id FROM XSLTemplate WHERE id=@id", this.m_dbConnection);
            cmd.Parameters.AddWithValue("@id", input.id.ToString("N", ITUtil.Common.Constants.Culture.DefaultCulture));
            var dbresult = cmd.ExecuteScalar();
            cmd.Dispose();
            if (dbresult != null)
            {
                cmd = new SQLiteCommand("UPDATE XSLTemplate SET name=@name, template=@template where id = @id", this.m_dbConnection);
                cmd.Parameters.AddWithValue("@id", input.id.ToString("N", ITUtil.Common.Constants.Culture.DefaultCulture));
                cmd.Parameters.AddWithValue("@name", input.name);
                cmd.Parameters.AddWithValue("@template", input.template);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// DeleteXSLTemplate
        /// </summary>
        /// <param name="input">ITUtil.XSLT.DTO.GetXSLTemplate input</param>
        /// <returns>bool</returns>
        public bool DeleteXSLTemplate(ITUtil.XSLT.DTO.GetXSLTemplate input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            var cmd = new SQLiteCommand("SELECT id FROM XSLTemplate WHERE id=@id", this.m_dbConnection);
            cmd.Parameters.AddWithValue("@id", input.id.ToString("N", ITUtil.Common.Constants.Culture.DefaultCulture));
            var dbresult = cmd.ExecuteScalar();
            cmd.Dispose();
            if (dbresult != null)
            {
                cmd = new SQLiteCommand("DELETE FROM XSLTemplate where id = @id", this.m_dbConnection);
                cmd.Parameters.AddWithValue("@id", input.id.ToString("N", ITUtil.Common.Constants.Culture.DefaultCulture));
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// ReadXSLTemplate
        /// </summary>
        /// <returns>ITUtil.XSLT.DTO.XSLTemplates</returns>
        public ITUtil.XSLT.DTO.XSLTemplates ReadXSLTemplate()
        {
            var cmd = new SQLiteCommand("SELECT id, name,template FROM XSLTemplate", this.m_dbConnection);

            var dbresult = cmd.ExecuteReader();
            cmd.Dispose();
            if (dbresult != null)
            {
                ITUtil.XSLT.DTO.XSLTemplates list = new ITUtil.XSLT.DTO.XSLTemplates();
                list.templates = new List<ITUtil.XSLT.DTO.XSLTemplate>();
                while (dbresult.Read())
                {
                    ITUtil.XSLT.DTO.XSLTemplate template = new ITUtil.XSLT.DTO.XSLTemplate();
                    template.id = new Guid(dbresult.GetString(0));
                    template.name = dbresult.GetString(1);
                    template.template = dbresult.GetString(2);
                    list.templates.Add(template);
                }

                return list;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// GenerateXML
        /// </summary>
        /// <param name="input">ITUtil.XSLT.DTO.XMLInput input</param>
        /// <returns>ITUtil.XSLT.DTO.XML</returns>
        public ITUtil.XSLT.DTO.XML GenerateXML(ITUtil.XSLT.DTO.XMLInput input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            var cmd = new SQLiteCommand("SELECT template FROM XSLTemplate WHERE id=@id", this.m_dbConnection);
            cmd.Parameters.AddWithValue("@id", input.xSLTemplateId.ToString("N", ITUtil.Common.Constants.Culture.DefaultCulture));
            var dbresult = cmd.ExecuteReader();

            if (dbresult != null && dbresult.Read())
            {
                System.Xml.XmlDocument orgDoc = new System.Xml.XmlDocument();
                orgDoc.LoadXml(input.xML);

                XmlNode transNode = orgDoc.SelectSingleNode("/");
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                XmlWriterSettings settings = new XmlWriterSettings();

                settings.ConformanceLevel = ConformanceLevel.Auto;
                XmlWriter writer = XmlWriter.Create(sb, settings);

                System.Xml.Xsl.XslCompiledTransform trans = new System.Xml.Xsl.XslCompiledTransform();
                trans.Load(XmlReader.Create(new StringReader(dbresult.GetString(0))));

                trans.Transform(transNode, writer);

                return new ITUtil.XSLT.DTO.XML() { content = sb.ToString() };

                // XDocument newTree = new XDocument();
                // using (StringReader sri = new StringReader(input.XML)) // xmlInput is a string that contains xml
                // {
                //    using (XmlReader xri = XmlReader.Create(sri))
                //    {
                //        using (XmlWriter writer = newTree.CreateWriter())
                //        {
                //            // Load the style sheet.
                //            XslCompiledTransform xslt = new XslCompiledTransform();
                //            xslt.Load(XmlReader.Create(new StringReader(dbresult.GetString(0))));
                //            // Execute the transform and output the results to a writer.
                //            xslt.Transform(xri, writer);
                //        }
                //    }
                // }
                // return new ITUtil.XSLT.DTO.XML() { Content = newTree.ToString() };
            }
            else
            {
                return null;
            }
        }
    }
}

﻿// <copyright file="UnitTest.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Unittests
{
    using System;
    using System.Linq;
    using NUnit.Framework;

    /// <summary>
    /// UnitTest
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
        /// <summary>
        /// CreateNewXSLTemplate
        /// </summary>
        [Test]
        public void CreateNewXSLTemplate()
        {
            Monosoft.Service.XSLT.Database_v1 db = new Monosoft.Service.XSLT.Database_v1();
            var res = db.CreateXSLTemplate(new ITUtil.XSLT.DTO.XSLTemplate() { id = Guid.NewGuid(), name = "test", template = "testtemplate" });
            Assert.IsTrue(res, "We should be able to create a new template");
        }

        /// <summary>
        /// CreateNewExistingXSLTemplate
        /// </summary>
        [Test]
        public void CreateNewExistingXSLTemplate()
        {
            Monosoft.Service.XSLT.Database_v1 db = new Monosoft.Service.XSLT.Database_v1();
            var id = Guid.NewGuid();
            db.CreateXSLTemplate(new ITUtil.XSLT.DTO.XSLTemplate() { id = id, name = "test", template = "testtemplate" });
            var res = db.CreateXSLTemplate(new ITUtil.XSLT.DTO.XSLTemplate() { id = id, name = "test", template = "testtemplate" });
            Assert.IsFalse(res, "Test should fail, as the template have been created");
        }

        /// <summary>
        /// UpdateNonExistingXSLTemplate
        /// </summary>
        [Test]
        public void UpdateNonExistingXSLTemplate()
        {
            Monosoft.Service.XSLT.Database_v1 db = new Monosoft.Service.XSLT.Database_v1();
            var res = db.UpdateXSLTemplate(new ITUtil.XSLT.DTO.XSLTemplate() { id = Guid.NewGuid(), name = "test", template = "testtemplate" });
            Assert.IsFalse(res, "We should not be able to update an non exisiting template");
        }

        /// <summary>
        /// UpdateExistingXSLTemplate
        /// </summary>
        [Test]
        public void UpdateExistingXSLTemplate()
        {
            Monosoft.Service.XSLT.Database_v1 db = new Monosoft.Service.XSLT.Database_v1();
            var id = Guid.NewGuid();
            db.CreateXSLTemplate(new ITUtil.XSLT.DTO.XSLTemplate() { id = id, name = "test", template = "testtemplate" });
            var res = db.UpdateXSLTemplate(new ITUtil.XSLT.DTO.XSLTemplate() { id = id, name = "test2", template = "testtemplate2" });
            Assert.IsTrue(res, "We should be able to update an existing template");
        }

        /// <summary>
        /// DeleteNonExistingXSLTemplate
        /// </summary>
        [Test]
        public void DeleteNonExistingXSLTemplate()
        {
            Monosoft.Service.XSLT.Database_v1 db = new Monosoft.Service.XSLT.Database_v1();
            var res = db.DeleteXSLTemplate(new ITUtil.XSLT.DTO.GetXSLTemplate() { id = Guid.NewGuid() });
            Assert.IsFalse(res, "We should not be able to delete an non exisiting template");
        }

        /// <summary>
        /// DeleteExistingXSLTemplate
        /// </summary>
        [Test]
        public void DeleteExistingXSLTemplate()
        {
            Monosoft.Service.XSLT.Database_v1 db = new Monosoft.Service.XSLT.Database_v1();
            var id = Guid.NewGuid();
            db.CreateXSLTemplate(new ITUtil.XSLT.DTO.XSLTemplate() { id = id, name = "test", template = "testtemplate" });
            var res = db.DeleteXSLTemplate(new ITUtil.XSLT.DTO.GetXSLTemplate() { id = id });
            Assert.IsTrue(res, "We should be able to delete an existing template");
        }

        /// <summary>
        /// ReadXSLTemplate
        /// </summary>
        [Test]
        public void ReadXSLTemplate()
        {
            Monosoft.Service.XSLT.Database_v1 db = new Monosoft.Service.XSLT.Database_v1();
            var id = Guid.NewGuid();
            db.CreateXSLTemplate(new ITUtil.XSLT.DTO.XSLTemplate() { id = id, name = "test", template = "testtemplate" });
            var res = db.ReadXSLTemplate();
            var amoutfound = res.templates.Where(p => p.id == id).Count();
            Assert.AreEqual(1, amoutfound, "We expect that there should be exactly one result");
        }

        /// <summary>
        /// ReadNonExistingXSLTemplate
        /// </summary>
        [Test]
        public void ReadNonExistingXSLTemplate()
        {
            Monosoft.Service.XSLT.Database_v1 db = new Monosoft.Service.XSLT.Database_v1();
            var id = Guid.NewGuid();
            var res = db.ReadXSLTemplate();
            var amoutfound = res.templates.Where(p => p.id == id).Count();
            Assert.AreEqual(0, amoutfound, "We expect that there should be exactly zero result");
        }
    }
}
